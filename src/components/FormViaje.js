import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from '../api/viajeros-viajes';

export const FormViaje = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [codigo, setCodigo] = useState('');
  const [origen, setOrigen] = useState('');
  const [destino, setDestino] = useState('');
  const [precio, setPrecio] = useState('');
  const [numeroPlazas, setNumPlazas] = useState('');

  const [errores, setErrores] = useState(null);
  const [mensajes, setMensajes] = useState('');

  useEffect(() => {
    if (id) {
      axios
        .get(`/viajes/${id}`)
        .then((res) => {
          const { data } = res.data;
          setCodigo(data.codigo);
          setOrigen(data.origen);
          setDestino(data.destino);
          setPrecio(data.precio);
          setNumPlazas(data.numeroPlazas);
        })
        .catch((err) => console.error(err));
    }
  }, []);

  const codigoChange = (e) => {
    setCodigo(e.target.value);
  };
  const origenChange = (e) => {
    setOrigen(e.target.value);
  };
  const destinoChange = (e) => {
    setDestino(e.target.value);
  };
  const precioChange = (e) => {
    setPrecio(e.target.value);
  };
  const numPlazasChange = (e) => {
    setNumPlazas(e.target.value);
  };

  const resetForm = () => {
    setCodigo('');
    setOrigen('');
    setDestino('');
    setPrecio('');
    setNumPlazas('');
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const datos = { codigo, origen, destino, numeroPlazas, precio };

    if (id) {
      axios
        .put(`/viajes/${id}`, datos)
        .then((res) => {
          const { message } = res.data;
          setMensajes(message);
          setErrores(null);
          if (message.includes('no fueron modificados')) {
            // navigate(-1);
          }
          if (message.includes('actualizado con exito')) {
            setTimeout(() => {
              navigate(-1);
            }, 3000);
          }
        })
        .catch((err) => {
          console.log('===ERROR===');
          const { data } = err.response;
          console.log(data);
          setErrores(data);
        });
    } else {
      if (codigo && origen && destino && numPlazasChange && precio) {
        axios
          .post('/viajes', datos)
          .then((res) => {
            setErrores(null);
            resetForm();
            navigate(-1);
          })
          .catch((err) => {
            console.log('===ERROR===');
            const { data } = err.response;
            setErrores(data);
          });
      } else {
        setMensajes('Debe ingresar datos al formulario');
        setErrores(null);
      }
    }
  };

  return (
    <div>
      <div className="card">
        <div className="card-body">
          <h5 className="card-title"> {id ? 'Editar Viaje' : 'Nuevo Viaje'} </h5>

          {mensajes ? <div className="p-3 mb-2 bg-info text-white">{mensajes}</div> : ''}

          {errores ? (
            <div className="p-3 mb-2 bg-danger text-white">
              {errores.message.detail ? errores.message.detail : errores.message}
            </div>
          ) : (
            ''
          )}

          <form onSubmit={handleSubmit}>
            <div className="row m-top">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  value={codigo}
                  onChange={codigoChange}
                  placeholder="Código"
                />
              </div>
            </div>

            <div className="row m-top">
              <div className="col-sm-12 col-md-6">
                <input
                  type="text"
                  className="form-control"
                  value={origen}
                  onChange={origenChange}
                  placeholder="Origen"
                />
              </div>
              <div className="col-sm-12 col-md-6">
                <input
                  type="text"
                  className="form-control"
                  value={destino}
                  onChange={destinoChange}
                  placeholder="Destino"
                />
              </div>
            </div>

            <div className="row m-top">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  value={precio}
                  onChange={precioChange}
                  placeholder="Precio"
                />
              </div>
            </div>

            <div className="row m-top">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  value={numeroPlazas}
                  onChange={numPlazasChange}
                  placeholder="Número de Plazas"
                />
              </div>
            </div>

            <div className="row m-top">
              <div className="col">
                <button type="submit" className="btn btn-primary float-right">
                  {id ? 'Guardar' : 'Agregar'}
                </button>
                <Link to={'/viajes'} className="btn btn-outline-info">
                  Regresar al Listado
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
