import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from '../api/viajeros-viajes';

export const FormViajero = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [cedula, setCedula] = useState('');
  const [nombre, setNombre] = useState('');
  const [fechaNacimiento, setFechaNacimiento] = useState('');
  const [telefono, setTelefono] = useState('');

  const [errores, setErrores] = useState(null);
  const [mensajes, setMensajes] = useState('');

  useEffect(() => {
    if (id) {
      axios
        .get(`/viajeros/${id}`)
        .then((res) => {
          const { data } = res.data;
          setCedula(data.cedula);
          setNombre(data.nombre);
          setFechaNacimiento(data.fechaNacimientoFormato);
          setTelefono(data.telefono);
        })
        .catch((err) => console.error(err));
    }
  }, []);

  const cedulaChange = (e) => {
    setCedula(e.target.value);
  };
  const nombreChange = (e) => {
    setNombre(e.target.value);
  };
  const fechaNacimientoChange = (e) => {
    setFechaNacimiento(e.target.value);
  };
  const telefonoChange = (e) => {
    setTelefono(e.target.value);
  };

  const resetForm = () => {
    setCedula('');
    setNombre('');
    setFechaNacimiento('');
    setTelefono('');
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const datos = { cedula, nombre, fechaNacimiento, telefono };

    if (id) {
      axios
        .put(`/viajeros/${id}`, datos)
        .then((res) => {
          const { message } = res.data;
          setMensajes(message);
          setErrores(null);
          if (message.includes('no fueron modificados')) {
            // navigate(-1);
          }
          if (message.includes('actualizado con exito')) {
            setTimeout(() => {
              navigate(-1);
            }, 3000);
          }
        })
        .catch((err) => {
          console.log('===ERROR===');
          const { data } = err.response;
          console.log(data);
          setErrores(data);
        });
    } else {
      if (cedula && nombre && fechaNacimiento && telefono) {
        axios
          .post('/viajeros', datos)
          .then((res) => {
            setErrores(null);
            resetForm();
            navigate(-1);
          })
          .catch((err) => {
            console.log('===ERROR===');
            const { data } = err.response;
            setErrores(data);
          });
      } else {
        setMensajes('Debe ingresar datos al formulario');
        setErrores(null);
      }
    }
  };

  return (
    <div>
      <div className="card">
        <div className="card-body">
          <h5 className="card-title"> {id ? 'Editar Viajero' : 'Nuevo Viajero'} </h5>

          {mensajes ? <div className="p-3 mb-2 bg-info text-white">{mensajes}</div> : ''}

          {errores ? (
            <div className="p-3 mb-2 bg-danger text-white">
              {errores.message.detail ? errores.message.detail : errores.message}
            </div>
          ) : (
            ''
          )}

          <form onSubmit={handleSubmit}>
            <div className="row m-top">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  value={cedula}
                  onChange={cedulaChange}
                  placeholder="Cédula"
                />
              </div>
            </div>

            <div className="row m-top">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  value={nombre}
                  onChange={nombreChange}
                  placeholder="Nombre"
                />
              </div>
            </div>

            <div className="row m-top">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  value={fechaNacimiento}
                  onChange={fechaNacimientoChange}
                  placeholder="Fecha de Nacimiento"
                />
              </div>
            </div>

            <div className="row m-top">
              <div className="col">
                <input
                  type="text"
                  className="form-control"
                  value={telefono}
                  onChange={telefonoChange}
                  placeholder="Teléfono"
                />
              </div>
            </div>

            <div className="row m-top">
              <div className="col">
                <button type="submit" className="btn btn-primary float-right">
                  {id ? 'Guardar' : 'Agregar'}
                </button>
                <Link to={'/viajeros'} className="btn btn-outline-info">
                  Regresar al Listado
                </Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
