import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from '../api/viajeros-viajes';

export const FormViajeroViaje = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  // const [viajero, setViajero] = useState(null);
  const [viajes, setViajes] = useState(null);
  const [viaje, setViaje] = useState('');

  const [errores, setErrores] = useState(null);
  const [mensajes, setMensajes] = useState('');

  useEffect(() => {
    if (id) {
      // axios
      //   .get(`/viajeros/${id}`)
      //   .then((res) => {
      //     const { data } = res.data;
      //     setViajero(data);
      //   })
      //   .catch((err) => console.error(err));
      axios
        .get(`/viajes`)
        .then((res) => {
          const { data } = res.data;
          setViajes(data);
        })
        .catch((err) => console.error(err));
    }
  }, []);

  const viajeChange = (e) => {
    setViaje(e.target.value);
    console.log(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (viaje) {
      axios
        .post('/viajeros/asignar_viajes', { viajeroId: id, viajesIds: [viaje] })
        .then((res) => {
          const { message } = res.data;
          console.log(message)
          setMensajes(message);
          setErrores(null);
          if (message.includes('asignaron con exito')) {
            setTimeout(() => {
              navigate(-1);
            }, 3000);
          }
        })
        .catch((err) => {
          console.log('===ERROR===');
          const { data } = err.response;
          setErrores(data);
        });
    } else {
      setMensajes('Debe seleccionar un viaje');
      setErrores(null);
    }
  };

  return (
    <div>
      <div className="card">
        <div className="card-body">
          <h5 className="card-title"> {id ? 'Editar Viajero' : 'Nuevo Viajero'} </h5>

          {mensajes ? <div className="p-3 mb-2 bg-info text-white">{mensajes}</div> : ''}

          {errores && (
            <div className="p-3 mb-2 bg-danger text-white">
              {errores.message.detail ? errores.message.detail : errores.message}
            </div>
          )}

          <form onSubmit={handleSubmit}>
            <div className="row m-top">
              <div className="col">
                <div className="form-group">
                  <label>Seleccione un Viaje</label>
                  <select className="form-control" value={viaje} onChange={viajeChange}>
                    <option value="0">Selecione un Viaje</option>
                    {viajes &&
                      viajes.map((v) => {
                        return (
                          <option key={v.id} value={v.id}>
                            [{v.codigo}] {v.origen}-{v.destino}
                          </option>
                        );
                      })}
                  </select>
                </div>
              </div>
            </div>

            <div className="row m-top">
              <div className="col">
                <button type="submit" className="btn btn-primary float-right">Asignar Viaje</button>
                <Link to={`/viajeros/${id}`} className="btn btn-outline-info">Regresar al Viajero</Link>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
