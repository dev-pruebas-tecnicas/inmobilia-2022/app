import { NavLink } from 'react-router-dom';
import './NavBar.css';

export const NavBar = () => {
  return (
    <div>
      <ul className="nav justify-content-center">
        <li className="nav-item">
          <NavLink className={({ isActive }) => (isActive ? 'nav-link active' : 'nav-link')} to="/">
            Home
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className={({ isActive }) => (isActive ? 'nav-link active' : 'nav-link')} to="/viajes">
            Viajes
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink className={({ isActive }) => (isActive ? 'nav-link active' : 'nav-link')} to="/viajeros">
            Viajeros
          </NavLink>
        </li>
      </ul>
    </div>
  );
};
