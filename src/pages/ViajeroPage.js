import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from '../api/viajeros-viajes';

export const ViajeroPage = () => {
  const [listado, setListado] = useState([]);

  useEffect(() => {
    axios.get('/viajeros').then((res) => {
      // console.log(res.data.data);
      setListado(res.data.data);
    });
  }, []);

  return (
    <div>
      <h1>Listado de Viajeros</h1>

      <Link to={"/viajeros/nuevo"} className="btn btn-info float-right">Agregar Nuevo Viajero</Link>

      <table className="table">
        <thead>
          <tr>
            <th className="text-center" scope="col">
              #
            </th>
            <th className="text-center" scope="col">
              CÉDULA
            </th>
            <th className="text-center" scope="col">
              NOMBRE
            </th>
            <th className="text-center" scope="col">
              CANTIDAD DE VIAJES
            </th>
            <th className="text-center" scope="col">
              ACCIONES
            </th>
          </tr>
        </thead>

        <tbody>
          {listado.map((l) => {
            return (
              <tr key={l.id}>
                <th className="text-center" scope="row"> {l.id} </th>
                <td className="text-center">{l.cedula}</td>
                <td className="text-center">{l.nombre}</td>
                <td className="text-center">{l.listadoViajes.length}</td>
                <td className="text-center">
                  <Link className='p-1' to={`/viajeros/${l.id}`}>ver</Link>
                  <Link className='p-1' to={`/viajeros/${l.id}/editar`}>editar</Link>
                  <Link className='p-1' to={`/viajeros/${l.id}/asignar-viaje`}>asignar</Link>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
