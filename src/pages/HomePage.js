import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from '../api/viajeros-viajes';

export const HomePage = () => {
  const [listado, setListado] = useState([]);

  useEffect(() => {
    axios.get('/viajero_viajes').then((res) => {
      // console.log(res.data.data);
      setListado(res.data.data);
    });
  }, []);

  return (
    <div>
      <div className="jumbotron jumbotron-fluid">
        <div className="container">
          <h1 className="display-4">Prueba Técnica</h1>
          <p className="lead">
            Una agencia de viajes desea automatizar la gestión de los clientes que acuden a su oficina y los viajes que
            estos realizan.
          </p>
        </div>
      </div>

      <h1>Listado de Viajes Realizados</h1>

      <table className="table">
        <thead>
          <tr>
            <th className="text-center" scope="col">
              #
            </th>
            <th className="text-center" scope="col">
              VIAJERO
            </th>
            <th className="text-center" scope="col">
              LUGAR ORIGEN
            </th>
            <th className="text-center" scope="col">
              LUGAR DESTINO
            </th>
          </tr>
        </thead>

        <tbody>
          {listado.map((l) => {
            return (
              <tr key={l.id}>
                <th className="text-center" scope="row">
                  {l.id}
                </th>
                <td className="text-center">
                  <Link to={`/viajeros/${l.viajero.id}`}>{l.viajero.nombre}</Link>
                </td>
                <td className="text-center">{l.viaje.origen}</td>
                <td className="text-center">{l.viaje.destino}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
