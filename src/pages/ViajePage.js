import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from '../api/viajeros-viajes';

export const ViajePage = () => {
  const [listado, setListado] = useState([]);

  useEffect(() => {
    axios.get('/viajes').then((res) => {
      // console.log(res.data.data);
      setListado(res.data.data);
    });
  }, []);

  return (
    <div>
      <h1>Listado de Viajes</h1>

      <Link to={"/viajes/nuevo"} className="btn btn-info float-right">Agregar Nuevo Viaje</Link>

      <table className="table">
        <thead>
          <tr>
            <th className="text-center" scope="col">
              #
            </th>
            <th className="text-center" scope="col">
              CÓDIGO
            </th>
            <th className="text-center" scope="col">
              ORIGEN
            </th>
            <th className="text-center" scope="col">
              DESTINO
            </th>
            <th className="text-center" scope="col">
              PRECIO
            </th>
            <th className="text-center" scope="col">
              ACCIONES
            </th>
          </tr>
        </thead>

        <tbody>
          {listado.map((l) => {
            return (
              <tr key={l.id}>
                <th className="text-center" scope="row"> {l.id} </th>
                <td className="text-center">{l.codigo}</td>
                <td className="text-center">{l.origen}</td>
                <td className="text-center">{l.destino}</td>
                <td className="text-center">{l.precio}</td>
                <td className="text-center">
                  <Link className='p-1' to={`/viajes/${l.id}`}>ver</Link>
                  <Link className='p-1' to={`/viajes/${l.id}/editar`}>editar</Link>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};
