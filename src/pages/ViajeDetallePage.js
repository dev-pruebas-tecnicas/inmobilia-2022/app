import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from '../api/viajeros-viajes';

import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

export const ViajeDetallePage = () => {
    const { id } = useParams();
    const navigate = useNavigate();

    const [detalle, setDetalle] = useState(null);

    const [errores, setErrores] = useState(null);
  
    useEffect(() => {
      axios
        .get(`/viajes/${id}`)
        .then((res) => {
          setDetalle(res.data.data);
        })
        .catch((err) => console.log(err));
    }, []);
  

    const activarAlerta = () => {
      const MySwal = withReactContent(Swal);

      MySwal.fire({
        title: '¿Seguro que desea borrar el Viaje?',
        showCancelButton: true,
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar',
      }).then((opc) => {
        setErrores(null);
        if (opc.isConfirmed) {
          axios
            .delete(`/viajes/${id}`)
            .then((res) => {
              navigate(-1);
            })
            .catch((err) => {
              console.log('===ERROR===');
              const { data } = err.response;
              console.log(data);
              setErrores(data);
            });
        }
      });
    };

    return (
      <div>
        <ul className="list-group">
          {detalle != null ? (
            <div>
              <h4>Detalles del Viaje</h4>

              {errores && (
              <div className="p-3 mb-2 bg-danger text-white">
                {errores.message.detail ? errores.message.detail : errores.message}
              </div>
            )}

              <li className="list-group-item"> <b>Código:</b> {detalle.codigo} </li>
              <li className="list-group-item"> <b>Origen:</b> {detalle.origen} </li>
              <li className="list-group-item"> <b>Destino:</b> {detalle.destino} </li>
              <li className="list-group-item"> <b>Precio:</b> {detalle.precio} </li>
              <li className="list-group-item"> <b>Número de Plazas:</b> {detalle.numeroPlazas} </li>
            </div>
          ) : (
            <div>
              <li className="list-group-item">
                <b>no se encontraron los datos</b>
              </li>
            </div>
          )}
        </ul>

        <div className='mt-3'>
          <button type="button" onClick={activarAlerta} className="btn btn-danger float-right">Borrar Viaje</button>
          <Link to={'/viajes'} className="btn btn-outline-info"> Regresar al Listado </Link>
        </div>
      </div>
    );
}
