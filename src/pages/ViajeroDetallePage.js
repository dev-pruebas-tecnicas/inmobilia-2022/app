import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from '../api/viajeros-viajes';

import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

export const ViajeroDetallePage = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [detalle, setDetalle] = useState(null);

  const [errores, setErrores] = useState(null);

  useEffect(() => {
    axios
      .get(`/viajeros/${id}`)
      .then((res) => {
        setDetalle(res.data.data);
        // console.log(res.data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const activarAlerta = () => {
    const MySwal = withReactContent(Swal);

    MySwal.fire({
      title: '¿Seguro que desea borrar al Viajero?',
      showCancelButton: true,
      confirmButtonText: 'Eliminar',
      cancelButtonText: 'Cancelar',
    }).then((opc) => {
      setErrores(null);
      if (opc.isConfirmed) {
        axios
          .delete(`/viajeros/${id}`)
          .then((res) => {
            navigate(-1);
          })
          .catch((err) => {
            console.log('===ERROR===');
            const { data } = err.response;
            console.log(data);
            setErrores(data);
          });
      }
    });
  };

  return (
    <div>
      <ul className="list-group">
        {detalle != null ? (
          <div>
            <h4>Detalles del Viajero</h4>

            {errores && (
              <div className="p-3 mb-2 bg-danger text-white">
                {errores.message.detail ? errores.message.detail : errores.message}
              </div>
            )}

            <li className="list-group-item">
              <b>Cédula:</b> {detalle.cedula}
            </li>
            <li className="list-group-item">
              <b>Nombre:</b> {detalle.nombre}
            </li>
            <li className="list-group-item">
              <b>Teléfono:</b> {detalle.telefono}
            </li>
            <li className="list-group-item">
              <b>Fecha de Nacimiento:</b> {detalle.fechaNacimientoFormato}
            </li>
            <li className="list-group-item">
              <b>Número de viajes:</b> {detalle.listadoViajes.length}
            </li>

            {detalle.listadoViajes.length > 0 && (
              <div className="mt-2">
                <p>Viajes Realizados:</p>
                {detalle.listadoViajes.map((l) => {
                  return (
                    <div key={l.id} className="alert alert-secondary" role="alert">
                      {l.viaje.origen} - {l.viaje.destino}
                    </div>
                  );
                })}
              </div>
            )}
          </div>
        ) : (
          <div>
            <li className="list-group-item">
              <b>no se encontraron los datos</b>
            </li>
          </div>
        )}
      </ul>

      <div className="mt-3">
        <button type="button" onClick={activarAlerta} className="btn btn-danger float-right">
          Borrar Viajero
        </button>
        <Link to={'/viajeros'} className="m-1 btn btn-outline-info">
          Regresar al Listado
        </Link>
        {detalle && (
        <Link to={`/viajeros/${detalle.id}/asignar-viaje`} className="m-1 btn btn-primary">
          Asignar Viajes
        </Link>
        )}
      </div>
    </div>
  );
};
