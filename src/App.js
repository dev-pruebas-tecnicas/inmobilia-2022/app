import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import { FormViaje } from './components/FormViaje';
import { FormViajero } from './components/FormViajero';
import { FormViajeroViaje } from './components/FormViajeroViaje';
import { HomePage } from './pages/HomePage';
import { NavBar } from './components/NavBar';
import { ViajeDetallePage } from './pages/ViajeDetallePage';
import { ViajePage } from './pages/ViajePage';
import { ViajeroDetallePage } from './pages/ViajeroDetallePage';
import { ViajeroPage } from './pages/ViajeroPage';

function App() {
  return (
    <Router>
      <div className="container">
        <NavBar />

        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/viajeros" element={<ViajeroPage />} />
          <Route path="/viajeros/nuevo" element={<FormViajero />} />
          <Route path="/viajeros/:id" element={<ViajeroDetallePage />} />
          <Route path="/viajeros/:id/editar" element={<FormViajero />} />
          <Route path="/viajeros/:id/asignar-viaje" element={<FormViajeroViaje />} />
          <Route path="/viajes" element={<ViajePage />} />
          <Route path="/viajes/nuevo" element={<FormViaje />} />
          <Route path="/viajes/:id" element={<ViajeDetallePage />} />
          <Route path="/viajes/:id/editar" element={<FormViaje />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
